package bitset

// Get bit from uint64
func Get(v uint64, pos int) bool {
	pos = Position(pos, 64)
	return v&(1<<pos) != 0
}

// Set bit to uint64
func Set(v uint64, pos int, ok bool) uint64 {
	pos = Position(pos, 64)
	if ok {
		return v | (1 << pos)
	}
	var tmp uint64 = ^(1 << pos)
	return v & tmp
}

// Position return position on array
func Position(pos, length int) int {
	pos %= length
	if pos < 0 {
		pos += length
	}
	return pos
}
