package bitset

import (
	"encoding/binary"
	"errors"
)

// ErrUnmarshal .
var ErrUnmarshal = errors.New(`can't unmarshal bitset`)

// Marshal uint64[length + data]
func Marshal(bits *Bitset, order binary.ByteOrder) []byte {
	var (
		length = bits.length
		count  = len(bits.data)
		data   = make([]byte, 8*(1+count))
	)
	order.PutUint64(data, uint64(length))
	tmp := data
	for i := 0; i < count; i++ {
		tmp = tmp[8:]
		order.PutUint64(tmp, bits.data[i])
	}
	return data
}

// Unmarshal from uint64[length + data]
func Unmarshal(data []byte, order binary.ByteOrder) (bits *Bitset, e error) {
	count := len(data)
	if count < 8 {
		e = ErrUnmarshal
		return
	}
	count -= 8

	length := order.Uint64(data)
	if length == 0 {
		bits = emptyBitset
		return
	}
	mod := length % 64
	min := length / 64
	if mod != 0 {
		min++
	}
	if uint64(count)/8 < min {
		e = ErrUnmarshal
		return
	}
	bits = New(int(length))
	for i := uint64(0); i < min; i++ {
		data = data[8:]
		bits.data[i] = order.Uint64(data)
	}
	return
}
