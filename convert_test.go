package bitset_test

import (
	"math"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/king011_go/bitset"
)

func TestConvert(t *testing.T) {
	for i := 0; i < 100; i++ {
		testBitset(t, bitset.New(i))
	}
}
func testConvert(t *testing.T, bits *bitset.Bitset) {
	length := bits.Len()
	var val uint64
	b := make([]byte, length)
	filp := make([]byte, length)
	for i := range b {
		b[i] = '0'
		filp[i] = '1'
	}
	for i := 0; i < bits.Len(); i++ {
		b[i] = '1'
		filp[i] = '0'
		str := bits.String()
		assert.Equal(t, str, string(b))
		other := bitset.FromString(str)
		assert.True(t, other.Equal(bits))
		other.FlipAll()
		str = other.String()
		assert.Equal(t, str, string(filp))
		other.FlipAll()
		assert.True(t, other.Equal(bits))
		str = other.String()
		assert.Equal(t, str, string(b))

		if i == 0 {
			val = 1
		} else if val < 64 {
			val <<= 1
			val++
		}
		bits.Set(i)
		if i < 8 {
			tmp := bits.ToUint8()
			assert.Equal(t, uint8(val), tmp)
			assert.Equal(t, bitset.FromUint8(tmp).ToUint8(), tmp)
		} else {
			tmp := bits.ToUint8()
			assert.Equal(t, math.MaxUint8, tmp)
			assert.Equal(t, bitset.FromUint8(tmp).ToUint8(), tmp)
		}
		if i < 16 {
			tmp := bits.ToUint16()
			assert.Equal(t, uint16(val), tmp)
			assert.Equal(t, bitset.FromUint16(tmp).ToUint16(), tmp)
		} else {
			tmp := bits.ToUint16()
			assert.Equal(t, math.MaxUint16, tmp)
			assert.Equal(t, bitset.FromUint16(tmp).ToUint16(), tmp)
		}
		if i < 32 {
			tmp := bits.ToUint32()
			assert.Equal(t, uint32(val), tmp)
			assert.Equal(t, bitset.FromUint32(tmp).ToUint32(), tmp)
		} else {
			tmp := bits.ToUint32()
			assert.Equal(t, math.MaxUint32, tmp)
			assert.Equal(t, bitset.FromUint32(tmp).ToUint32(), tmp)
		}
		if i < 64 {
			tmp := bits.ToUint64()
			assert.True(t, val == tmp)
			assert.True(t, bitset.FromUint64(tmp).ToUint64() == tmp)
		} else {
			tmp := bits.ToUint64()
			assert.True(t, math.MaxUint64 == tmp)
			assert.True(t, bitset.FromUint64(tmp).ToUint64() == tmp)
		}
	}
}
