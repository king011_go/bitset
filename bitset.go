package bitset

var emptyBitset = &Bitset{}

// Bitset set<bool> implement by bytes
type Bitset struct {
	length int
	data   []uint64
}

// New Bitset
func New(length int) *Bitset {
	if length <= 0 {
		return emptyBitset
	}
	var data []uint64
	if length > 0 {
		mod := length % 64
		count := length / 64
		if mod != 0 {
			count++
		}
		data = make([]uint64, count)
	}
	return &Bitset{
		length: length,
		data:   data,
	}
}

// Len return bits length
func (bits *Bitset) Len() int {
	return bits.length
}

// Cap return memory capacity
func (bits *Bitset) Cap() int {
	return len(bits.data)
}

// Position return position on array
func (bits *Bitset) Position(pos int) int {
	return Position(pos, bits.length)
}

// Test if all bits set return true
func (bits *Bitset) Test(pos ...int) bool {
	for _, i := range pos {
		i = bits.Position(i)
		if !Get(bits.data[i/64], i%64) {
			return false
		}
	}
	return true
}

// None if no bit set return true
func (bits *Bitset) None(pos ...int) bool {
	for _, i := range pos {
		i = bits.Position(i)
		if Get(bits.data[i/64], i%64) {
			return false
		}
	}
	return true
}

// Any if any bits set return true
func (bits *Bitset) Any(pos ...int) bool {
	for _, i := range pos {
		i = bits.Position(i)
		if Get(bits.data[i/64], i%64) {
			return true
		}
	}
	return false
}

// Set pos bits value
func (bits *Bitset) Set(pos ...int) *Bitset {
	var offset, bit int
	for _, i := range pos {
		i = bits.Position(i)
		offset = i / 64
		bit = i % 64
		bits.data[offset] = Set(bits.data[offset], bit, true)
	}
	return bits
}

// Reset pos bits value
func (bits *Bitset) Reset(pos ...int) *Bitset {
	var offset, bit int
	for _, i := range pos {
		i = bits.Position(i)
		offset = i / 64
		bit = i % 64
		bits.data[offset] = Set(bits.data[offset], bit, false)
	}
	return bits
}

// SetAll bits value
func (bits *Bitset) SetAll(ok bool) *Bitset {
	var offset, bit int
	for i := 0; i < bits.length; i++ {
		offset = i / 64
		bit = i % 64
		bits.data[offset] = Set(bits.data[offset], bit, ok)
	}
	return bits
}

// Flip bits
func (bits *Bitset) Flip(pos ...int) *Bitset {
	var offset, bit int
	for _, i := range pos {
		i = bits.Position(i)
		offset = i / 64
		bit = i % 64
		bits.data[offset] = Set(bits.data[offset], bit, !Get(bits.data[offset], bit))
	}
	return bits
}

// FlipAll bits
func (bits *Bitset) FlipAll() *Bitset {
	for i, v := range bits.data {
		bits.data[i] = ^v
	}
	return bits
}

// Equal compare bits equal
func (bits *Bitset) Equal(other *Bitset) bool {
	var lcount, rcount int
	if bits != nil {
		lcount = bits.length
	}
	if other != nil {
		rcount = bits.length
	}
	if lcount != rcount {
		return false
	} else if lcount == 0 {
		return true
	}
	count := len(bits.data)
	max := count - 1
	for i := 0; i < max; i++ {
		if bits.data[i] != other.data[i] {
			return false
		}
	}
	l := bits.data[max]
	r := other.data[max]
	mod := lcount % 64
	if mod == 0 {
		return l == r
	}
	mov := 64 - mod
	l <<= mov
	l >>= mov
	r <<= mov
	r >>= mov
	return l == r
}
