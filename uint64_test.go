package bitset_test

import (
	"math"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/king011_go/bitset"
)

func TestUint64(t *testing.T) {
	var value uint64
	for i := 0; i < 64; i++ {
		if i == 0 {
			value = 1
		} else {
			value *= 2
		}
		x := bitset.Set(0, i, true)
		assert.Equal(t, x, value)

		for j := 0; j < 64; j++ {
			if i == j {
				assert.True(t, bitset.Get(x, j))
			} else {
				assert.False(t, bitset.Get(x, j))
			}
		}
	}
	for i := 0; i < 64; i++ {
		if i == 0 {
			value = 1
		} else {
			value *= 2
		}
		x := bitset.Set(math.MaxUint64, i, false)
		assert.Equal(t, ^x, value)
		for j := 0; j < 64; j++ {
			if i == j {
				assert.False(t, bitset.Get(x, j))
			} else {
				assert.True(t, bitset.Get(x, j))
			}
		}
	}
}

func TestPosition(t *testing.T) {
	for length := 0; length < 100; length++ {
		for i := 0; i < length*3; i++ {
			assert.Equal(t, i%length, bitset.Position(i, length))
			if i != 0 {
				mod := i % length
				if mod == 0 {
					assert.Equal(t, 0, bitset.Position(-i, length))
				} else {
					assert.Equal(t, length-mod, bitset.Position(-i, length))
				}
			}
		}
	}
}
