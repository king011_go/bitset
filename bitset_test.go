package bitset_test

import (
	"encoding/binary"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/king011_go/bitset"
)

func TestNew(t *testing.T) {
	bits := bitset.New(0)
	assert.Equal(t, bits.Len(), 0)
	assert.Equal(t, bits.Cap(), 0)

	size := 64
	for j := 0; j < 12; j++ {
		for i := size*j + 1; i <= size*(j+1); i++ {
			bits = bitset.New(i)
			assert.Equal(t, bits.Len(), i)
			assert.Equal(t, bits.Cap(), j+1)
		}
	}
}
func TestBitset(t *testing.T) {
	for i := 0; i < 100; i++ {
		testBitset(t, bitset.New(i))
	}
}
func testBitset(t *testing.T, bits *bitset.Bitset) {
	length := bits.Len()
	b := make([]byte, length)
	filp := make([]byte, length)
	for i := range b {
		b[i] = '0'
		filp[i] = '1'
	}
	for i := 0; i < length; i++ {
		index := length - i - 1
		b[index] = '1'
		filp[index] = '0'

		testBitsetValue(t, bits, i, false)
		bits.Set(i)
		testBitsetValue(t, bits, i, true)

		assert.Equal(t, bits.String(), string(b))

		testBitsetValue(t, bits, i, true)
		bits.Flip(i)
		testBitsetValue(t, bits, i, false)
		b[index] = '0'
		assert.Equal(t, bits.String(), string(b))
		bits.FlipAll()
		filp[index] = '1'
		assert.Equal(t, bits.String(), string(filp))
		filp[index] = '0'
		testBitsetFlipValue(t, bits, i, true)
		bits.FlipAll()
		assert.Equal(t, bits.String(), string(b))
		bits.Flip(i)
		testBitsetValue(t, bits, i, true)
		b[index] = '1'
		assert.Equal(t, bits.String(), string(b))

		bits.FlipAll()
		assert.Equal(t, bits.String(), string(filp))
		testBitsetFlipValue(t, bits, i, false)
		bits.FlipAll()
		testBitsetValue(t, bits, i, true)
	}
	bits.SetAll(false)
	for i := range b {
		b[i] = '0'
	}
	assert.Equal(t, bits.String(), string(b))
	bits.SetAll(true)
	for i := range b {
		b[i] = '1'
	}
	assert.Equal(t, bits.String(), string(b))
}
func testBitsetFlipValue(t *testing.T, bits *bitset.Bitset, i int, ok bool) {
	var pos []int
	for j := 0; j < i; j++ {
		assert.False(t, bits.Test(j))
		assert.True(t, bits.None(j))
		assert.False(t, bits.Any(j))
		pos = append(pos, j)
	}
	assert.True(t, bits.None(pos...))
	assert.False(t, bits.Any(pos...))
	if len(pos) == 0 {
		assert.True(t, bits.Test(pos...))
	} else {
		assert.False(t, bits.Test(pos...))
	}

	pos = append(pos, i)
	if ok {
		if i == 0 {
			assert.True(t, bits.Test(pos...))
		} else {
			assert.False(t, bits.Test(pos...))
		}
		assert.False(t, bits.None(pos...))
		assert.True(t, bits.Any(pos...))

		assert.True(t, bits.Test(i))
		assert.False(t, bits.None(i))
		assert.True(t, bits.Any(i))
	} else {
		assert.False(t, bits.Test(pos...))
		assert.True(t, bits.None(pos...))
		assert.False(t, bits.Any(pos...))

		assert.False(t, bits.Test(i))
		assert.True(t, bits.None(i))
		assert.False(t, bits.Any(i))
	}
}
func testBitsetValue(t *testing.T, bits *bitset.Bitset, i int, ok bool) {
	var pos []int
	for j := 0; j < i; j++ {
		assert.True(t, bits.Test(j))
		assert.False(t, bits.None(j))
		assert.True(t, bits.Any(j))
		pos = append(pos, j)
	}
	assert.True(t, bits.Test(pos...))
	if len(pos) == 0 {
		assert.True(t, bits.None(pos...))
		assert.False(t, bits.Any(pos...))
	} else {
		assert.False(t, bits.None(pos...))
		assert.True(t, bits.Any(pos...))
	}
	pos = append(pos, i)
	if ok {
		assert.True(t, bits.Test(pos...))
		assert.False(t, bits.None(pos...))
		assert.True(t, bits.Any(pos...))

		assert.True(t, bits.Test(i))
		assert.False(t, bits.None(i))
		assert.True(t, bits.Any(i))
	} else {
		assert.False(t, bits.Test(pos...))
		if i == 0 {
			assert.True(t, bits.None(pos...))
			assert.False(t, bits.Any(pos...))
		} else {
			assert.False(t, bits.None(pos...))
			assert.True(t, bits.Any(pos...))
		}

		assert.False(t, bits.Test(i))
		assert.True(t, bits.None(i))
		assert.False(t, bits.Any(i))
	}
}
func TestEqual(t *testing.T) {
	for i := 0; i < 100; i++ {
		testBitsetEqual(t, bitset.New(i), bitset.New(i))
	}
}
func testBitsetEqual(t *testing.T, l, r *bitset.Bitset) {
	length := l.Len()
	assert.True(t, l.Equal(r))
	tmp := bitset.New(length)
	for i := 0; i < tmp.Len(); i++ {
		tmp.Set(i)
	}
	for i := 0; i < length; i++ {
		l.Set(i)
		assert.False(t, l.Equal(r))
		if l.Equal(r) {
			t.Fatal(l, r)
		}
		r.Set(i)
		assert.True(t, l.Equal(r))

		testMarshal(t, l)
		tmp.Reset(i)
		l.FlipAll()
		testMarshal(t, l)
		assert.True(t, l.Equal(tmp))
		if !l.Equal(tmp) {
			l.Equal(tmp)
			t.Fatal()
		}
		l.FlipAll()
		assert.True(t, l.Equal(r))
	}
}
func testMarshal(t *testing.T, bits *bitset.Bitset) {
	var (
		order binary.ByteOrder
		data  []byte
	)
	for i := 0; i < 2; i++ {
		if i == 0 {
			order = binary.BigEndian
		} else {
			order = binary.LittleEndian
		}
		data = bitset.Marshal(bits, order)
		assert.Equal(t, len(data), 8*(1+bits.Cap()))
		assert.Equal(t, bits.Len(), int(order.Uint64(data)))
		tmp, e := bitset.Unmarshal(data, order)
		assert.Nil(t, e)
		assert.True(t, bits.Equal(tmp))
	}
}
