package bitset

import (
	"errors"
	"strconv"
)

// ErrNotBinaryString from string error
var ErrNotBinaryString = errors.New(`it's not binary string`)

func (bits *Bitset) String() string {
	if bits.length == 0 {
		return ``
	}
	b := make([]byte, bits.length)
	for i := 0; i < bits.length; i++ {
		if bits.Test(i) {
			b[bits.length-1-i] = '1'
		} else {
			b[bits.length-1-i] = '0'
		}
	}
	return string(b)
}
func min(l, r int) int {
	if l < r {
		return l
	}
	return r
}
func toUint64(v uint64, bits int) uint64 {
	if bits > 0 {
		mov := 64 - bits
		v >>= mov
		v <<= mov
	}
	return v
}

// ToUint .
func (bits *Bitset) ToUint() uint {
	if bits.length == 0 {
		return 0
	}
	return uint(toUint64(bits.data[0], min(strconv.IntSize, bits.length)))
}

// ToUint8 .
func (bits *Bitset) ToUint8() uint8 {
	if bits.length == 0 {
		return 0
	}
	return uint8(toUint64(bits.data[0], min(8, bits.length)))
}

// ToUint16 .
func (bits *Bitset) ToUint16() uint16 {
	if bits.length == 0 {
		return 0
	}
	return uint16(toUint64(bits.data[0], min(16, bits.length)))
}

// ToUint32 .
func (bits *Bitset) ToUint32() uint32 {
	if bits.length == 0 {
		return 0
	}
	return uint32(toUint64(bits.data[0], min(32, bits.length)))
}

// ToUint64 .
func (bits *Bitset) ToUint64() uint64 {
	if bits.length == 0 {
		return 0
	}
	return toUint64(bits.data[0], min(64, bits.length))
}

// FromString .
func FromString(str string) *Bitset {
	bits, e := TryFromString(str)
	if e != nil {
		panic(e)
	}
	return bits
}

// TryFromString .
func TryFromString(str string) (*Bitset, error) {
	bits := New(len(str))
	for i := 0; i < bits.length; i++ {
		if str[0] == '1' {
			bits.Set(i)
		} else if str[0] != '0' {
			return nil, ErrNotBinaryString
		}
	}
	return bits, nil
}

// FromUint .
func FromUint(v uint) *Bitset {
	bits := New(strconv.IntSize)
	bits.data[0] = uint64(v)
	return bits
}

// FromUint8 .
func FromUint8(v uint8) *Bitset {
	bits := New(8)
	bits.data[0] = uint64(v)
	return bits
}

// FromUint16 .
func FromUint16(v uint16) *Bitset {
	bits := New(16)
	bits.data[0] = uint64(v)
	return bits
}

// FromUint32 .
func FromUint32(v uint32) *Bitset {
	bits := New(32)
	bits.data[0] = uint64(v)
	return bits
}

// FromUint64 .
func FromUint64(v uint64) *Bitset {
	bits := New(64)
	bits.data[0] = v
	return bits
}
